!!Este proyecto se ha probado utilizando node v14.16.0

* Servidor websocket.

Para desplegar el servidor nodejs websocket debe ubicarse mediante una consola de comandos en la ruta ./socket/
A continuación debe ejecutar el comando npm install para instalar todas las librerías
Al finalizar debe ejecutar el comando npm start para iniciar el servidor web socket
El servidor estará escuchando en la url localhost:1000


* React app

Para desplegar la aplicacion react debe ubicarse en la ruta ./pruebamvm/ mediante consola de comandos
A continuación debe ejecutar el comando npm install para instalar todas las librerías
Después del paso anterior ejecute de nuevo el comando npm start
La aplicación podrá observarse en la url localhost:3000