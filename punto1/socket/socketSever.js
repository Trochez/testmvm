

const fs = require('fs');
const io = require('socket.io')();
io.on('connection', socket => {
    console.log(`connect: ${socket.id}`);

    socket.on('disconnect', () => {
      console.log(`disconnect: ${socket.id}`);
    });

    fs.readFile('valores.txt','utf8',(err,data)=>{
      if(err){
        console.log("ERROR  ")
        io.emit('message', "ERROR");
      }
      else{
        console.log("data  "+data)
        io.emit('message', data);
      }
    })
});

fs.watch('valores.txt', (eventType, filename) => {
  console.log(`event type is: ${eventType}`);
  if (filename) {
    console.log(`filename provided: ${filename}`);

    fs.readFile(filename,'utf8',(err,data)=>{
      if(err){
        console.log("ERROR  ")
        io.emit('message', "ERROR");
      }
      else{
        console.log("data  "+data)
        io.emit('message', data);
      }
    })

  } else {
    console.log('filename not provided');
  }
});


io.listen(1000, {
  cors: {
  }
});

