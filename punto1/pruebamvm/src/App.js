import React, { Component } from 'react';
import io from 'socket.io-client';

const socket = io("localhost:1000"); // replace with the IP of your server, when testing on real devices

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      connected: socket.connected,
      currentTransport: socket.connected ? socket.io.engine.transport.name : '-',
      lastMessage: ""
    };
    
  }

  componentDidMount() {
    socket.on('connect', () => this.onConnectionStateUpdate());
    socket.on('disconnect', () => this.onConnectionStateUpdate());
    socket.on('message', (content) => this.onMessage(content));
  }

  componentWillUnmount() {
    socket.off('connect');
    socket.off('disconnect');
    socket.off('message');
  }

  onConnectionStateUpdate() {
    this.setState({
      connected: socket.connected,
      currentTransport: socket.connected ? socket.io.engine.transport.name : '-'
    });
    if (socket.connected) {
      socket.io.engine.on('upgrade', () => this.onUpgrade());
    } else {
      socket.io.engine.off('upgrade');
    }
  }

  onMessage(content) {
    this.setState({
      lastMessage: content
    });
  }

  onUpgrade() {
    this.setState({
      currentTransport: socket.io.engine.transport.name
    });
  }

  getTable(message){

    var messageIn = message.replaceAll(","," ").split('\n');
    var head = true;

    return <table>
            <tbody>
              <tr>
              {
                  messageIn[0].split(" ").map(function(column){
                    return <th>{column}</th>
                  })
                    
              }
              </tr>
            {
              
              messageIn.map(function(row){

                if(head){
                  head = false;
                  return ;
                }

                var columns = row.split(" ");
                return <tr>{
                  columns.map(function(column){
                    return <td>{column}</td>
                  })
                  }</tr>
              })
              
            }
            </tbody>
    </table> 

  }

  render() {

    console.log("message  "+this.state.lastMessage)

    var table = this.getTable(this.state.lastMessage)

    return (
        <div className="divTable">
          <div>
          </div>
          {table}
        </div>
    );
  }
}
