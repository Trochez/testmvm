var express = require('express')
const cors = require('cors')
var http = require('http')
var https = require('https')
var path = require('path')
const sqlite3 = require('sqlite3').verbose();


var bodyParser = require('body-parser')
var app = express()
const expressSwagger = require('express-swagger-generator')(app);


var process = require('./process.json')
const { response } = require('express')


let options = {
    swaggerDefinition: {
        info: {
            description: 'testmvm swagger',
            title: 'Swagger',
            version: '1.0.0',
        },
        host: 'localhost:5000',
        basePath: '/',
        produces: [
            "application/json",
            "application/xml"
        ],
        schemes: ['http'],
        securityDefinitions: {
            JWT: {
                type: 'apiKey',
                in: 'header',
                name: 'Authorization',
                description: "",
            }


        }
    },
    basedir: __dirname, //app absolute path
    files: ['./back.js'] //Path to the API handle folder
}

//app.use(sqlinjection)
app.use(bodyParser.json()) // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })) // support encoded bodies
app.use(cors({ origin: true }))
app.use(express.static(path.join(__dirname, 'public')))
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  next()
})
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

let db = new sqlite3.Database(process.env.database, (err) => {
    if (err) {
        return console.error(err.message);
    }

    db.run('CREATE TABLE IF NOT EXISTS users (first_name TEXT NOT NULL,last_name TEXT NOT NULL,email TEXT NOT NULL UNIQUE,phone TEXT NOT NULL);', [], function(err) {
        if (err) {
            console.log("ERROR creando tabla usuarios")
        }
       
    }
    );
    console.log('Connected to SQlite database.');
});




/**
 * This function get the users from database
 * @route GET /getUsers
 * @group users
 * @returns {Array.<User>} User - users list 
 * @returns {Error}  default - Unexpected error
 */

app.get('/getUsers', function (req, res) {

    var ms = ""

    try{

        db.all("select * from users", [], (err, rows) => {
            if (err) {
                console.log("ERROR   "+JSON.stringify(err))
                ms = "Error obteniendo usuarios"
                res.send(ms)
            }

            else{
                ms = JSON.stringify(rows)
                res.send(ms)
            }
        
        });

    
    }
    catch(err){
        ms = JSON.stringify(err)
        res.send(ms)
    }

    
    return
	  

})

/**
 * JSON parameters to saveUser with parameter
 * @typedef User
 * @property {string} first_name - user name - eg:juan
 * @property {string} last_name - user lastname - eg:tróchez
 * @property {string} email - user email - eg: juan.trochez@correounivalle.edu.co
 * @property {string} phone - user tel - eg:3157713040
 */


/**
 * This function save user in database
 * @route Post /saveUser
 * @param {User.model} req.body - user info
 * @group users
 * @returns {string} 200 - message
 * @returns {Error}  default - Unexpected error
 */

app.post('/saveUser', function (req, res) {

    console.log("in saveUser  "+JSON.stringify(req.body))


    var ms = ""

	var fieldnames = []
	var fieldvalues = []
	var paramvalue = ""



	for(k in req.body){

		paramvalue +="?,"

		fieldnames.push(k)
		fieldvalues.push(req.body[k])

	}

	paramvalue = paramvalue.substring(0,paramvalue.length-1)


	try{
        //var statment = db.prepare('INSERT INTO users ('+fieldnames+') VALUES ('+paramvalue+')')

        //console.log("sql   "+statment.sql)

        db.run( 'INSERT INTO users ('+fieldnames+') VALUES ('+paramvalue+')',fieldvalues, function(err) {
            if (err) {
                ms = "Error guardando usuario, es posible que el email digitado ya este registrado"
                console.log("ERROR svae user   "+JSON.stringify(err.message)+"  sql   "+db.sql)
                res.send(JSON.stringify(ms))
            }
            else{
                ms = "Usuario guardado exitosamente" 
                res.send(JSON.stringify(ms))
            }

            console.log("ms save user   "+JSON.stringify(ms))

            
            return;
           
        }
        );

	}
	catch(err){
		ms = JSON.stringify(err)
		console.log("err "+JSON.stringify(err))
        res.send(ms)
        return;
	}
    
	
  

})



/**
 * This function update user in database
 * @route Put /updateUser
 * @param {User.model} req.body - user info
 * @group users
 * @returns {string} 200 - message
 * @returns {Error}  default - Unexpected error
 */

app.put('/updateUser', function (req, res) {

    //console.log("req.body update   "+JSON.stringify(req.body))


    var ms = ""

    var fieldupdate = []

    var paramupdate = ""



	for(k in req.body){

        paramupdate += k+" = ?,"

		fieldupdate.push(req.body[k])
	


	}

	paramupdate = paramupdate.substring(0,paramupdate.length-1)

    


	try{

        var statement = db.prepare("update users set "+paramupdate+" where email = ? ")

		statement.run(fieldupdate.concat([req.body.email]), function(err) {

            //console.log("statement.sql  "+statement.sql)

            if (err) {
                console.log("ERROR   "+JSON.stringify(err))
                ms = "Error actualizando usuario, es posible que el email digitado ya este registrado"
                res.send(JSON.stringify(ms))
            }
            else{
                ms = "Usuario actualizado exitosamente"
                console.log(ms)
                res.send(JSON.stringify(ms))
            }
           
        }
        );

	}
	catch(err){
		ms = JSON.stringify(err)
		console.log("err "+JSON.stringify(err.message))
        res.send(ms)
		
	}
    
	return;
  

})



/**
 * This function remove an users from database
 * @route DELETE /removeUser
 * @param {User.model} req.body - user info
 * @group users
 * @returns {string} 200 - users list 
 * @returns {Error}  default - Unexpected error
 */

app.delete('/removeUser', function (req, res) {

    //console.log("in remove user   "+JSON.stringify(req.body))

    var ms="";

    ms = "Error actualizando usuario"
    try{

        db.run("delete from users where email = ?", req.body.email, function(err) {
            if (err) {
                ms = "Error eliminando usuario"
                res.send(JSON.stringify(ms))
            }
            else{
                ms = "Usuario eliminado exitosamente"
                res.send(JSON.stringify(ms))
            }
        });

    }
    catch(err){
        ms = JSON.stringify(err)
        res.send(ms)
    }
    
    return
  

})

app.get('/',function(req,res){

    res.send("all up");
    return;

})


expressSwagger(options)

app.listen(process.env.PORT || 5000, function () {
  console.log('Example app listening on port '+process.env.port+'!')
})

