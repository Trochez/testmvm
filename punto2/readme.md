!!Este proyecto se ha probado utilizando node v14.16.0

* Servidor nodejs.

Para desplegar el servidor nodejs debe ubicarse mediante una consola de comandos en la ruta ./back/
A continuación debe ejecutar el comando npm install para instalar las librerías
Por último ejecute el comando npm start para iniciar la aplicación
El servidor estará escuchando en la url localhost:5000
Una interfaz swagger para probar los servicios sera visible en la url localhost:5000/api-docs

La aplicación utiliza una base de datos sqlite para almacenar la información en el archivo llamado users.db ubicado en la carpeta ./back/


* React app

Para desplegar la aplicacion react debe ubicarse en la ruta ./pruebamvm2/ mediante consola de comandos
Despu[es debe ejecutar el comando npm install para instalar las libreías
Después del paso anterior ejecute de nuevo el comando npm start
La aplicación podrá observarse en la url localhost:3000