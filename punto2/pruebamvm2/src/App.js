import React, { Component } from 'react';


export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users:[],
      table:<table></table>
    };

    this.updatevalue = "";
    
  }

  componentDidMount() {
    this.getUsers();
  }


  async getUsers(){

    console.log("in getusers")
    var ths = this;

    await fetch('http://localhost:5000/getUsers')
      .then(response =>response.json())
      .then(data => {
        if(typeof data == 'undefined'){
          data = []
        }
        this.setState({
          users: data
        });
      });
  }

  async createUser(){

    var user = {
      first_name:prompt("Digite el nombre del usuario"),
      last_name:prompt("Digite el apellido del usuario"),
      email:prompt("Digite el email del usuario"),
      phone:prompt("Digite el teléfono del usuario")
    }

    if(user.email.indexOf("@")<0||user.email.indexOf(".")<0){
      user.email=prompt("El email digitado es incorrecto, digite un email válido");
    }

    console.log("user to save   "+JSON.stringify(user))

    fetch('http://localhost:5000/saveUser',{
      method:'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body:JSON.stringify(user)
    })
    .then(response => response.json())
    .then(data => {
      console.log("data createUser   "+data)
      alert(data);
      this.getUsers();
    });
     

  }

  async updateUser(user,key,e){

    if(this.updatevalue == e.target.value || e.target.value == ""){
      return;
    }

    this.updatevalue = e.target.value;

    if(key == "email" && (e.target.value.indexOf("@")<0||e.target.value.indexOf(".")<0)){
      alert("Email incorrecto ");
      return;
    }

    user[key] = e.target.value

    console.log("user to update   "+JSON.stringify(user))

    fetch('http://localhost:5000/updateUser',{
      method:'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body:JSON.stringify(user)
    })
    .then(response => response.json())
    .then(data => {
      console.log("data updateUser   "+data)
      alert(data);

      this.updateFlag = false;
    });
     

  }

  async removeUser(user){

    console.log("user to remove   "+JSON.stringify(user))

    fetch('http://localhost:5000/removeUser',{
      method:'DELETE',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body:JSON.stringify(user)
    })
    .then(response => response.json())
    .then(data => {
      console.log("data removeuser   "+data)
      this.getUsers();
      alert(data);
    });
     

  }

  async updateUserEnter(user,key,e){

    console.log("in enter")

    if(this.updatevalue == e.target.value){
      return;
    }

    if (e.key === 'Enter') {
      await this.updateUser(user,key,e);
    }
  }

  getTable(){

    //console.log("this.state.users   "+JSON.stringify(this.state.users))
    var ths = this;

    console.log("in get table")

    return <table>
            <thead>
              <tr>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Email</th>
                <th>Teléfono</th>
                <th>Eliminar</th>
              </tr>
            </thead>
            <tbody>
            {
              
              this.state.users.map(function(row){

                console.log("row   "+JSON.stringify(row))

                return <tr>{

                  Object.keys(row).map(function(key) {
                    //console.log("key  "+key+"  value   "+row[key])
                    if(key == "email"){
                      return <td><input value={row[key]} /></td>
                    }
                    else{
                      return <td><input placeholder={row[key]} onKeyUp={(e)=>ths.updateUserEnter(row,key,e)} onBlur={(e)=>ths.updateUser(row,key,e)}/></td>
                    }
                  })
                  
                }
                <td><label className="eliminar" onClick={(e)=>ths.removeUser(row)}>Eliminar</label></td>
                </tr>
              })
              
            }
            </tbody>
    </table> 

  }

  render() {

    console.log("in render  ")

    var table = this.getTable();
    return (
        <div className="divTable">
          <div>
            <label className="create" onClick={this.createUser.bind(this)}>crear usuario +</label>
          </div>
          <div>
            {table}
          </div>
        </div>
    );
  }
}
